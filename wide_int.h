#ifndef WIDE_INTEGER_H
#define WIDE_INTEGER_H

#ifdef _WIN32
#include <intrin.h>
#else
#include <x86intrin.h>
#endif

#include <stdint.h>

typedef int8_t  s8;
typedef uint8_t u8;

typedef int64_t  s64;
typedef uint64_t u64;

#ifdef __AVX2__
typedef __m256i __wide_int;

inline bool wide_int_equal(const __wide_int& a, const __wide_int& b) {
    return ((_mm256_extract_epi64(a, 0) == _mm256_extract_epi64(b, 0)) &&
            (_mm256_extract_epi64(a, 1) == _mm256_extract_epi64(b, 1)) &&
            (_mm256_extract_epi64(a, 2) == _mm256_extract_epi64(b, 2)) &&
            (_mm256_extract_epi64(a, 3) == _mm256_extract_epi64(b, 3)));
}

#ifdef _WIN32
inline __wide_int operator +(const __wide_int& a, const __wide_int& b) { 
    return _mm256_add_epi64(a, b);
}

inline __wide_int& operator +=(__wide_int& a, const __wide_int& b) { 
    return (a = a + b);
}
#endif

inline s64 hadd_wide_int(const __wide_int &a) {
    return (_mm256_extract_epi64(a, 0) +
            _mm256_extract_epi64(a, 1) +
            _mm256_extract_epi64(a, 2) +
            _mm256_extract_epi64(a, 3));
}

inline __wide_int load_wide_int_remainder(size_t size, const uint8_t *view) {
    u64 sum0 = 0, sum1 = 0, sum2 = 0, sum3 = 0;
    switch(size % 32) {
        case  0: sum0 |= ((u64)(*view++) <<  0);
        case 31: sum0 |= ((u64)(*view++) <<  8);
        case 30: sum0 |= ((u64)(*view++) << 16);
        case 29: sum0 |= ((u64)(*view++) << 24);
        case 28: sum0 |= ((u64)(*view++) << 32);
        case 27: sum0 |= ((u64)(*view++) << 40);
        case 26: sum0 |= ((u64)(*view++) << 48);
        case 25: sum0 |= ((u64)(*view++) << 56);
        case 24: sum1 |= ((u64)(*view++) <<  0);
        case 23: sum1 |= ((u64)(*view++) <<  8);
        case 22: sum1 |= ((u64)(*view++) << 16);
        case 21: sum1 |= ((u64)(*view++) << 24);
        case 20: sum1 |= ((u64)(*view++) << 32);
        case 19: sum1 |= ((u64)(*view++) << 40);
        case 18: sum1 |= ((u64)(*view++) << 48);
        case 17: sum1 |= ((u64)(*view++) << 56);
        case 16: sum2 |= ((u64)(*view++) <<  0);
        case 15: sum2 |= ((u64)(*view++) <<  8);
        case 14: sum2 |= ((u64)(*view++) << 16);
        case 13: sum2 |= ((u64)(*view++) << 24);
        case 12: sum2 |= ((u64)(*view++) << 32);
        case 11: sum2 |= ((u64)(*view++) << 40);
        case 10: sum2 |= ((u64)(*view++) << 48);
        case  9: sum2 |= ((u64)(*view++) << 56);
        case  8: sum3 |= ((u64)(*view++) <<  0);
        case  7: sum3 |= ((u64)(*view++) <<  8);
        case  6: sum3 |= ((u64)(*view++) << 16);
        case  5: sum3 |= ((u64)(*view++) << 24);
        case  4: sum3 |= ((u64)(*view++) << 32);
        case  3: sum3 |= ((u64)(*view++) << 40);
        case  2: sum3 |= ((u64)(*view++) << 48);
        case  1: sum3 |= ((u64)(*view++) << 56);
    }
    __wide_int result = _mm256_set_epi64x(sum3, sum2, sum1, sum0);
    return result;
}

#define __wide_int_load_si(var) _mm256_load_si256(var)

#elif (defined(_M_AMD64) || defined(_M_X64) || defined(__SSE2__))
typedef __m128i __wide_int;

inline bool wide_int_equal(__wide_int a, __wide_int b) {
    return ((((s64 *)&a)[0] == ((s64 *)&b)[0]) &&
            (((s64 *)&a)[1] == ((s64 *)&b)[1]));
}

#ifdef _WIN32
inline __wide_int operator +(const __wide_int& a, const __wide_int& b) { 
    return _mm_add_epi64(a, b);
}

inline __wide_int& operator +=(__wide_int& a, const __wide_int& b) { 
    return (a = a + b);
}
#endif

inline s64 hadd_wide_int(const __wide_int &a) {
    return ((((s64 *)&a)[0] + ((s64 *)&a)[1]));
}

inline __wide_int load_wide_int_remainder(size_t size, const uint8_t *view) {
    u64 sum0 = 0, sum1 = 0;
    switch(size % 16) {
        case  0: sum0 |= ((u64)(*view++) <<  0);
        case 15: sum0 |= ((u64)(*view++) <<  8);
        case 14: sum0 |= ((u64)(*view++) << 16);
        case 13: sum0 |= ((u64)(*view++) << 24);
        case 12: sum0 |= ((u64)(*view++) << 32);
        case 11: sum0 |= ((u64)(*view++) << 40);
        case 10: sum0 |= ((u64)(*view++) << 48);
        case  9: sum0 |= ((u64)(*view++) << 56);
        case  8: sum1 |= ((u64)(*view++) <<  0);
        case  7: sum1 |= ((u64)(*view++) <<  8);
        case  6: sum1 |= ((u64)(*view++) << 16);
        case  5: sum1 |= ((u64)(*view++) << 24);
        case  4: sum1 |= ((u64)(*view++) << 32);
        case  3: sum1 |= ((u64)(*view++) << 40);
        case  2: sum1 |= ((u64)(*view++) << 48);
        case  1: sum1 |= ((u64)(*view++) << 56);
    }
    __wide_int result = _mm_set_epi64x(sum1, sum0);
    return result;
}

#define __wide_int_load_si(var) _mm_load_si128(var)

#else // No support for wide integers, fallback to 64bit integer

typedef int64_t __wide_int;

inline bool wide_int_equal(const __wide_int& a, const __wide_int& b) {
    return (a == b);
}

#define hadd_wide_int(var) (var)

inline __wide_int load_wide_int_remainder(size_t size, const uint8_t *view) {
    u64 sum0 = 0, sum1 = 0;
    switch(size % 8) {
        case 0: sum0 |= ((u64)(*view++) <<  0);
        case 7: sum0 |= ((u64)(*view++) <<  8);
        case 6: sum0 |= ((u64)(*view++) << 16);
        case 5: sum0 |= ((u64)(*view++) << 24);
        case 4: sum0 |= ((u64)(*view++) << 32);
        case 3: sum0 |= ((u64)(*view++) << 40);
        case 2: sum0 |= ((u64)(*view++) << 48);
        case 1: sum0 |= ((u64)(*view++) << 56);
    }
    __wide_int result = sum0;
    return result;
}

#define __wide_int_load_si(var) (*var)

#endif // Support for wide integers (AVX2/SSE2)

#endif // WIDE_INTEGER_H
