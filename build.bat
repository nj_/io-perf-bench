@echo off

where /q cl
IF ERRORLEVEL 1 (
  call "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
)

cl io_perf_bench.cpp /nologo /EHsc /O2 /Z7 /Febench /arch:AVX2