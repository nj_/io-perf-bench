// A simple program to compare different ways to process in a very simple way
// full binary files, comparing win32 vs libc vs stl implementations
//    - Noam Jacobi

// To compile, use 'cl io_perf_bench.cpp /EHsc /O2 /Z7 /Febench'

// ----------------
#define KB(x) (1024LL*(x))
#define MB(x) (1024LL*KB(x))
#define GB(x) (1024LL*MB(x))

#if defined(_WIN32)
#include <windows.h>
#include <psapi.h>
#elif defined(__linux__)
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <dirent.h>
#include <time.h>
#include <libgen.h>
#include <linux/limits.h>
#else
static_assert(false);
#endif

#define eprintf(format, ...) fprintf(stderr, "%s: " format, __FUNCTION__, __VA_ARGS__)

#include <stdio.h>
#include <fstream>
#include <assert.h>

#include "defer.h"
#include "wide_int.h"
#include "work_loop.cpp"

// ----------------
// Program Settings
// ----------------
static const s64 test_run_count = 10;

struct TestResults {
    u64 performance_count = 0;
    u64 performance_time  = 0;
    u64 prefetch_faults   = 0;
    u64 manual_faults     = 0;
};

//
// Benchmark Procedures
//

#define BENCHMARK_PROC_SIG(name) s64 bench_##name(const char *file_name, size_t buffer_size, s64 run_count, TestResults *test_results)

#if defined(_WIN32)
BENCHMARK_PROC_SIG(mmap) {
    HANDLE file_handle;
    file_handle = CreateFileA(file_name, GENERIC_READ, FILE_SHARE_READ,
                              0, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, 0);
    
    if(file_handle == INVALID_HANDLE_VALUE) {
        eprintf("Could not open the file '%s'\n", file_name);
        return 0;
    }
    defer { CloseHandle(file_handle); };
    
    LARGE_INTEGER file_size;
    if(!GetFileSizeEx(file_handle, &file_size)) {
        eprintf("Could not open the file '%s'\n", file_name);
        return 0;
    }
    
    size_t size = file_size.QuadPart;
    
    HANDLE mapping_handle = CreateFileMapping(file_handle, NULL, PAGE_READONLY | SEC_COMMIT, 0, 0, NULL);
    defer { CloseHandle(mapping_handle); };
    
    const char *view = (const char *)MapViewOfFile(mapping_handle, FILE_MAP_READ, 0, 0, 0);
    defer { UnmapViewOfFile(view); };
    
    PROCESS_MEMORY_COUNTERS counter0, counter1;
    GetProcessMemoryInfo(GetCurrentProcess(), &counter0, sizeof(counter0));
    
    __wide_int first_result = work_loop(view, size);
    for(int i = 0; i < run_count - 1; ++i) {
        __wide_int result = work_loop(view, size);
        assert(wide_int_equal(first_result, result));
    }
    GetProcessMemoryInfo(GetCurrentProcess(), &counter1, sizeof(counter1));
    test_results->manual_faults = counter1.PageFaultCount - counter0.PageFaultCount;
    
    s64 sum = hadd_wide_int(first_result);
    return sum;
}

BENCHMARK_PROC_SIG(prefetch) {
    HANDLE file_handle;
    file_handle = CreateFileA(file_name, GENERIC_READ, FILE_SHARE_READ,
                              0, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, 0);
    
    if(file_handle == INVALID_HANDLE_VALUE) {
        eprintf("Could not open the file '%s'\n", file_name);
        return 0;
    }
    defer { CloseHandle(file_handle); };
    
    LARGE_INTEGER file_size;
    if(!GetFileSizeEx(file_handle, &file_size)) {
        eprintf("Could not open the file '%s'\n", file_name);
        return 0;
    }
    
    size_t size = file_size.QuadPart;
    
    HANDLE mapping_handle = CreateFileMapping(file_handle, NULL, PAGE_READONLY | SEC_COMMIT, 0, 0, NULL);
    defer { CloseHandle(mapping_handle); };
    
    const char *view = (const char *)MapViewOfFile(mapping_handle, FILE_MAP_READ, 0, 0, 0);
    defer { UnmapViewOfFile(view); };
    
    PROCESS_MEMORY_COUNTERS counter0, counter1, counter2;
    GetProcessMemoryInfo(GetCurrentProcess(), &counter0, sizeof(counter0));
    
    WIN32_MEMORY_RANGE_ENTRY memory_range_entry[] = {(void *)view, size};
    PrefetchVirtualMemory(GetCurrentProcess(), 1, memory_range_entry, 0);
    GetProcessMemoryInfo(GetCurrentProcess(), &counter1, sizeof(counter1));
    
    __wide_int first_result = work_loop(view, size);
    for(int i = 0; i < run_count - 1; ++i) {
        __wide_int result = work_loop(view, size);
        assert(wide_int_equal(first_result, result));
    }
    GetProcessMemoryInfo(GetCurrentProcess(), &counter2, sizeof(counter2));
    test_results->prefetch_faults = counter1.PageFaultCount - counter0.PageFaultCount;
    test_results->manual_faults   = counter2.PageFaultCount - counter1.PageFaultCount;
    
    s64 sum = hadd_wide_int(first_result);
    return sum;
}

BENCHMARK_PROC_SIG(read) {
    HANDLE file_handle;
    file_handle = CreateFileA(file_name, GENERIC_READ, FILE_SHARE_READ,
                              0, OPEN_EXISTING, 0, 0);
    
    if(file_handle == INVALID_HANDLE_VALUE) {
        eprintf("Could not open the file '%s'\n", file_name);
        return 0;
    }
    
    defer { CloseHandle(file_handle); };
    
    char *view = (char *)malloc(buffer_size);
    defer { free(view); };
    
    PROCESS_MEMORY_COUNTERS counter0, counter1;
    GetProcessMemoryInfo(GetCurrentProcess(), &counter0, sizeof(counter0));
    
    __wide_int first_result;
    for(int i = 0; i < run_count; ++i) {
        __wide_int result = {};
        DWORD size;
        SetFilePointer(file_handle, 0, 0, FILE_BEGIN);
        ReadFile(file_handle, view, buffer_size, &size, 0);
        while(size > 0) {
            result += work_loop(view, size);
            ReadFile(file_handle, view, buffer_size, &size, 0);
        }
        if(i == 0) first_result = result;
        else assert(wide_int_equal(first_result, result));
    }
    
    GetProcessMemoryInfo(GetCurrentProcess(), &counter1, sizeof(counter1));
    test_results->manual_faults = counter1.PageFaultCount - counter0.PageFaultCount;
    
    s64 sum = hadd_wide_int(first_result);
    return sum;
}
#elif defined(__linux__) 
BENCHMARK_PROC_SIG(mmap) {
    FILE *file = fopen(file_name, "r");
    
    if(file == nullptr) {
        eprintf("Could not open the file '%s'\n", file_name);
        return 0;
    }
    defer { fclose(file); };
    
    struct stat file_stats;
    int file_handle = fileno(file);
    if (fstat(file_handle, &file_stats) != 0) {
        eprintf("Could not get the file '%s' stats\n", file_name);
        return 0;
    }
    
    size_t file_size = file_stats.st_size;
    
    void *mapping_handle = mmap(nullptr, file_size, PROT_READ, MAP_PRIVATE | MAP_POPULATE, file_handle, 0);
    if(mapping_handle == MAP_FAILED) {
        eprintf("Could not map the file '%s' into memory\n", file_name);
        return 0;
    }
    defer { munmap(mapping_handle, file_size); };
    
    const char *view = (const char *)mapping_handle;
    
    struct rusage counter0, counter1;
    getrusage(RUSAGE_SELF, &counter0);
    
    __wide_int first_result = work_loop(view, file_size);
    for(int i = 0; i < run_count - 1; ++i) {
        __wide_int result = work_loop(view, file_size);
        assert(wide_int_equal(first_result, result));
    }
    
    getrusage(RUSAGE_SELF, &counter1);
    test_results->manual_faults = counter1.ru_majflt - counter0.ru_majflt;
    
    s64 sum = hadd_wide_int(first_result);
    return sum;
}
#endif

BENCHMARK_PROC_SIG(libc) {
    FILE *f = fopen(file_name, "rb");
    if(f == NULL) {
        eprintf("Could not open the file '%s'\n", file_name);
        return 0;
    }
    
    defer { fclose(f); };
    
    char *view = (char *)malloc(buffer_size);
    defer { free(view); };
    
#if defined(_WIN32)
    PROCESS_MEMORY_COUNTERS counter0, counter1;
    GetProcessMemoryInfo(GetCurrentProcess(), &counter0, sizeof(counter0));
#elif defined(__linux__) 
    struct rusage counter0, counter1;
    getrusage(RUSAGE_SELF, &counter0);
#endif
    
    __wide_int first_result;
    for(s64 i = 0; i < run_count; ++i) {
        fseek(f, 0, SEEK_SET);
        __wide_int result = {};
        size_t size = fread(view, 1, buffer_size, f);
        while (size > 0) {
            result += work_loop(view, size);
            size = fread(view, 1, buffer_size, f);
        }
        if(i == 0) first_result = result;
        else assert(wide_int_equal(first_result, result));
    }
    
#if defined(_WIN32)
    GetProcessMemoryInfo(GetCurrentProcess(), &counter1, sizeof(counter1));
    test_results->manual_faults = counter1.PageFaultCount - counter0.PageFaultCount;
#elif defined(__linux__) 
    getrusage(RUSAGE_SELF, &counter1);
    test_results->manual_faults = counter1.ru_majflt - counter0.ru_majflt;
#endif
    
    s64 sum = hadd_wide_int(first_result);
    return sum;
}

BENCHMARK_PROC_SIG(stl) {
    using namespace std;
    
    ifstream f(file_name, ifstream::binary);
    if(!f.good()) {
        eprintf("Could not open the file '%s'\n", file_name);
        return 0;
    }
    defer { f.close(); };
    
    char *view = (char *)malloc(buffer_size);
    defer { free(view); };
    
#if defined(_WIN32)
    PROCESS_MEMORY_COUNTERS counter0, counter1;
    GetProcessMemoryInfo(GetCurrentProcess(), &counter0, sizeof(counter0));
#elif defined(__linux__) 
    struct rusage counter0, counter1;
    getrusage(RUSAGE_SELF, &counter0);
#endif
    
    __wide_int first_result;
    for(s64 i = 0; i < run_count; ++i) {
        f.clear();
        f.seekg(0, f.beg);
        
        __wide_int result = {};
        f.read(view, buffer_size);
        size_t size = f.gcount();
        while (size > 0) {
            result += work_loop(view, size);
            f.read(view, buffer_size);
            size = f.gcount();
        }
        
        if(i == 0) first_result = result;
        else assert(wide_int_equal(first_result, result));
    }
    
#if defined(_WIN32)
    GetProcessMemoryInfo(GetCurrentProcess(), &counter1, sizeof(counter1));
    test_results->manual_faults = counter1.PageFaultCount - counter0.PageFaultCount;
#elif defined(__linux__) 
    getrusage(RUSAGE_SELF, &counter1);
    test_results->manual_faults = counter1.ru_majflt - counter0.ru_majflt;
#endif
    
    s64 sum = hadd_wide_int(first_result);
    return sum;
}

void benchmark_file(const char *file_path, const char *file_name, size_t size) {
    char size_string[16] = {};
    if(size > GB(1))      sprintf(size_string, "%2.2lf GB", (double)size/(double)GB(1));
    else if(size > MB(1)) sprintf(size_string, "%3.1lf MB", (double)size/(double)MB(1));
    else if(size > KB(1)) sprintf(size_string, "%4llu KB", (u64)size/KB(1));
    else                  sprintf(size_string, "%4llu B ", (u64)size);
    
    printf("%6s | %lld runs | '%s':\n", size_string, test_run_count, file_name);
    
    s64 first_result;
    bool first_bench = true;
    s64 test_index = 0;
    
#if defined(_WIN32)
    LARGE_INTEGER performance_counter_frequency;
    QueryPerformanceFrequency(&performance_counter_frequency);
    
#define TEST_REOPEN_HANDLE(name, file_name, run_count, buffer_size) \
do { \
printf("\n    %-14s (%lldKB multiple handles)\n", #name ": ", buffer_size/KB(1)); \
TestResults test_results; \
LARGE_INTEGER begin_time, end_time; \
u64 begin_counter, end_counter; \
s64 result = bench_##name(file_name, buffer_size, 1, &test_results); /* the first time does not count (need to warm up caches) */ \
if(first_bench) { first_bench = false; first_result = result; } \
QueryPerformanceCounter(&begin_time); \
begin_counter = __rdtsc(); \
for(int i = 0; i < test_run_count; ++i) { \
assert(bench_##name(file_name, buffer_size, 1, &test_results) == first_result); \
} \
end_counter = __rdtsc(); \
QueryPerformanceCounter(&end_time); \
test_results.performance_count = (end_counter - begin_counter); \
test_results.performance_time  = (end_time.QuadPart - begin_time.QuadPart); \
printf("    %-14s %12llu (ticks)\n",  " ", test_results.performance_count/test_run_count); \
printf("    %-14s %.10f (seconds)\n", " ", (float)test_results.performance_time/(float)performance_counter_frequency.QuadPart); \
printf("    %-14s %12lld (prefetch faults)\n", " ", test_results.prefetch_faults); \
printf("    %-14s %12lld (manual faults)\n",   " ", test_results.manual_faults); \
test_index++; \
} while(0)
    
#define TEST_SINGLE_HANDLE(name, file_name, run_count, buffer_size) \
do { \
printf("\n    %-14s (%lldKB single handle)\n", #name ": ", buffer_size/KB(1)); \
LARGE_INTEGER begin_time, end_time; \
TestResults test_results; \
u64 begin_counter, end_counter; \
s64 result = bench_##name(file_name, buffer_size, 1, &test_results); /* the first time does not count (need to warm up caches) */ \
if(first_bench) { first_bench = false; first_result = result; } \
QueryPerformanceCounter(&begin_time); \
begin_counter = __rdtsc(); \
assert(bench_##name(file_name, buffer_size, test_run_count, &test_results) == first_result); \
end_counter = __rdtsc(); \
QueryPerformanceCounter(&end_time); \
test_results.performance_count = (end_counter - begin_counter); \
test_results.performance_time  = (end_time.QuadPart - begin_time.QuadPart); \
printf("    %-14s %12llu (ticks)\n",  " ", test_results.performance_count/test_run_count); \
printf("    %-14s %.10f (seconds)\n", " ", (float)test_results.performance_time/(float)performance_counter_frequency.QuadPart); \
printf("    %-14s %12lld (prefetch faults)\n", " ", test_results.prefetch_faults); \
printf("    %-14s %12lld (manual faults)\n",   " ", test_results.manual_faults); \
test_index++; \
} while(0)
    
#elif defined(__linux__) 
    struct timespec performance_counter_frequency;
    clock_getres(CLOCK_MONOTONIC, &performance_counter_frequency);
    
#define TEST_REOPEN_HANDLE(name, file_name, run_count, buffer_size) \
do { \
printf("\n    %-14s (%lldKB multiple handles)\n", #name ": ", buffer_size/KB(1)); \
TestResults test_results; \
struct timespec begin_time, end_time; \
u64 begin_counter, end_counter; \
s64 result = bench_##name(file_name, buffer_size, 1, &test_results); /* the first time does not count (need to warm up caches) */ \
if(first_bench) { first_bench = false; first_result = result; } \
clock_gettime(CLOCK_MONOTONIC, &begin_time); \
begin_counter = __rdtsc(); \
for(int i = 0; i < test_run_count; ++i) { \
assert(bench_##name(file_name, buffer_size, 1, &test_results) == first_result); \
} \
end_counter = __rdtsc(); \
clock_gettime(CLOCK_MONOTONIC, &end_time); \
test_results.performance_count = (end_counter - begin_counter); \
test_results.performance_time  = (end_time.tv_sec - begin_time.tv_sec)*10e9 + (end_time.tv_nsec - begin_time.tv_nsec); \
printf("    %-14s %12llu (ticks)\n",  " ", test_results.performance_count/test_run_count); \
printf("    %-14s %.10f (seconds)\n", " ", (float)test_results.performance_time/(float)performance_counter_frequency.tv_nsec/(float)10e9); \
printf("    %-14s %12lld (prefetch faults)\n", " ", test_results.prefetch_faults); \
printf("    %-14s %12lld (manual faults)\n",   " ", test_results.manual_faults); \
test_index++; \
} while(0)
    
#define TEST_SINGLE_HANDLE(name, file_name, run_count, buffer_size) \
do { \
printf("\n    %-14s (%lldKB single handle)\n", #name ": ", buffer_size/KB(1)); \
struct timespec begin_time, end_time; \
TestResults test_results; \
u64 begin_counter, end_counter; \
s64 result = bench_##name(file_name, buffer_size, 1, &test_results); /* the first time does not count (need to warm up caches) */ \
if(first_bench) { first_bench = false; first_result = result; } \
clock_gettime(CLOCK_MONOTONIC, &begin_time); \
begin_counter = __rdtsc(); \
assert(bench_##name(file_name, buffer_size, test_run_count, &test_results) == first_result); \
end_counter = __rdtsc(); \
clock_gettime(CLOCK_MONOTONIC, &end_time); \
test_results.performance_count = (end_counter - begin_counter); \
test_results.performance_time  = (end_time.tv_sec - begin_time.tv_sec)*10e9 + (end_time.tv_nsec - begin_time.tv_nsec); \
printf("    %-14s %12llu (ticks)\n",  " ", test_results.performance_count/test_run_count); \
printf("    %-14s %.10f (seconds)\n", " ", (float)test_results.performance_time/(float)performance_counter_frequency.tv_nsec/(float)10e9); \
printf("    %-14s %12lld (prefetch faults)\n", " ", test_results.prefetch_faults); \
printf("    %-14s %12lld (manual faults)\n",   " ", test_results.manual_faults); \
test_index++; \
} while(0)
#endif
    
    printf("Testing reopening the file handle/reallocating memory:\n");
#if defined(_WIN32)
    TEST_REOPEN_HANDLE(prefetch, file_path, run_count, KB(0));
#endif
    TEST_REOPEN_HANDLE(mmap,     file_path, run_count, KB(0));
#if defined(_WIN32)
    TEST_REOPEN_HANDLE(read,     file_path, run_count, KB(512));
#endif
    TEST_REOPEN_HANDLE(libc,     file_path, run_count, KB(512));
    TEST_REOPEN_HANDLE(stl,      file_path, run_count, KB(512));
    
    printf("\nTesting reusing the file handle/memory:\n");
#if defined(_WIN32)
    TEST_SINGLE_HANDLE(prefetch, file_path, run_count, KB(0));
#endif
    TEST_SINGLE_HANDLE(mmap,     file_path, run_count, KB(0));
#if defined(_WIN32)
    TEST_SINGLE_HANDLE(read,     file_path, run_count, KB(512));
#endif
    TEST_SINGLE_HANDLE(libc,     file_path, run_count, KB(512));
    TEST_SINGLE_HANDLE(stl,      file_path, run_count, KB(512));
}

int main(int argc, char *argv[]) {
    if(argc != 2) {
        printf("Usage: bench <path>\n");
        return 0;
    }
    
    char *test_path = argv[1];
    
#if defined(_WIN32)
    DWORD file_attributes = GetFileAttributes(test_path);
    if(file_attributes == INVALID_FILE_ATTRIBUTES) {
        eprintf("Could not find '%s'\n", test_path);
        return 0;
    }
    
    bool is_directory = (file_attributes & FILE_ATTRIBUTE_DIRECTORY);
    
    char test_name_pattern[MAX_PATH];
    if(is_directory) sprintf(test_name_pattern, "%s\\*", test_path);
    else sprintf(test_name_pattern, "%s", test_path);
    
    WIN32_FIND_DATA find_data;
    HANDLE find_handle = FindFirstFile(test_name_pattern, &find_data);
    defer { FindClose(find_handle); };
    
    do {
        if (find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) continue;
        LARGE_INTEGER size_;
        size_.LowPart  = find_data.nFileSizeLow;
        size_.HighPart = find_data.nFileSizeHigh;
        
        size_t size = size_.QuadPart;
        const char *file_name = find_data.cFileName;
        
        char file_path[MAX_PATH];
        if(is_directory) sprintf(file_path, "%s\\%s", test_path, file_name);
        else             sprintf(file_path, "%s", test_path);
        
        benchmark_file(file_path, file_name, size);
    } while(FindNextFile(find_handle, &find_data) != 0);
#elif defined(__linux__)
    struct stat test_stats;
    if(stat(test_path, &test_stats) != 0) {
        eprintf("Could not find '%s'\n", test_path);
        return 0;
    }
    
    bool is_directory = test_stats.st_mode & S_IFDIR;
    
    char *full_test_path = realpath(test_path, nullptr);
    defer { free(full_test_path); };
    
    if(!is_directory) {
        const char *file_name = basename(test_path);
        benchmark_file(full_test_path, file_name, test_stats.st_size);
    } else {
        char test_name_pattern[PATH_MAX];
        sprintf(test_name_pattern, "%s", test_path);
        DIR *directory_handle = opendir(test_path);
        struct dirent *directory_entry;
        
        while(true) {
            directory_entry = readdir(directory_handle);
            if(!directory_entry) break;
            if(directory_entry->d_type != DT_REG) continue;
            
            char file_path[PATH_MAX];
            char *file_name = directory_entry->d_name;
            sprintf(file_path, "%s/%s", full_test_path, file_name);
            
            struct stat file_stats;
            if(stat(file_path, &file_stats) != 0) {
                eprintf("Could not get the file '%s' stats\n", file_name);
                continue;
            }
            
            benchmark_file(file_path, file_name, file_stats.st_size);
        }
    }
#endif
    
    return 0;
}