#ifndef DEFER_H
#define DEFER_H

// Taken from:
// https://gist.github.com/andrewrk/ffb272748448174e6cdb4958dae9f3d8#file-microsoft_craziness-h-L121

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundefined-internal"
#endif

#define CONCAT_INTERNAL(x,y) x##y
#define CONCAT(x,y) CONCAT_INTERNAL(x,y)

template<typename T>
struct ExitScope {
    T lambda;
    ExitScope(T lambda):lambda(lambda){}
    ~ExitScope(){lambda();}
    ExitScope(const ExitScope&);
    private:
    ExitScope& operator =(const ExitScope&);
};

class ExitScopeHelp {
    public:
    template<typename T>
        ExitScope<T> operator+(T t){ return t;}
};

#define defer const auto& CONCAT(defer__, __LINE__) = ExitScopeHelp() + [&]()

#ifdef __clang__
#pragma clang diagnostic pop
#endif

#endif //DEFER_H
