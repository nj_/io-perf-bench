// This file provides the work to be done on the file
// for the benchmarks
//    - Noam Jacobi

#include "wide_int.h"

#define DUFFS_MACHINE8(size, operation, var) \
do { \
switch(size % 8) { \
case -1: \
case  0: operation(var); \
case  7: operation(var); \
case  6: operation(var); \
case  5: operation(var); \
case  4: operation(var); \
case  3: operation(var); \
case  2: operation(var); \
case  1: operation(var); \
} \
} while(0)

#define DUFFS_MACHINE16(size, operation, var) \
do { \
switch(size % 16) { \
case -1: \
case  0: operation(var); \
case 15: operation(var); \
case 14: operation(var); \
case 13: operation(var); \
case 12: operation(var); \
case 11: operation(var); \
case 10: operation(var); \
case  9: operation(var); \
case  8: operation(var); \
case  7: operation(var); \
case  6: operation(var); \
case  5: operation(var); \
case  4: operation(var); \
case  3: operation(var); \
case  2: operation(var); \
case  1: operation(var); \
} \
} while(0)

#define DUFFS_MACHINE32(size, operation, var) \
do { \
switch(size % 32) { \
case -1: \
case  0: operation(var); \
case 31: operation(var); \
case 30: operation(var); \
case 29: operation(var); \
case 28: operation(var); \
case 27: operation(var); \
case 26: operation(var); \
case 25: operation(var); \
case 24: operation(var); \
case 23: operation(var); \
case 22: operation(var); \
case 21: operation(var); \
case 20: operation(var); \
case 19: operation(var); \
case 18: operation(var); \
case 17: operation(var); \
case 16: operation(var); \
case 15: operation(var); \
case 14: operation(var); \
case 13: operation(var); \
case 12: operation(var); \
case 11: operation(var); \
case 10: operation(var); \
case  9: operation(var); \
case  8: operation(var); \
case  7: operation(var); \
case  6: operation(var); \
case  5: operation(var); \
case  4: operation(var); \
case  3: operation(var); \
case  2: operation(var); \
case  1: operation(var); \
} \
} while(0)

__wide_int work_loop(const char *view, size_t size) {
    __wide_int result = {};
    
    const __wide_int *wide_view = (__wide_int *)view;
    s64 loop_size = (size - 1)/sizeof(__wide_int);
    
#define OP(var) result += __wide_int_load_si((var)++)
    
    s64 huge_loop_size = (loop_size - 1)/16;
    for(int i = 0; i < huge_loop_size; ++i) {
        // Seems like it helps a little, for some reason. Not sure why.
        _mm_prefetch((const char *)(wide_view + 17), _MM_HINT_T1);
        
        // We can reuse our DUFFS_MACHINE macro, and because -1 is a constant
        // the optimizer will remove the unnecessary jumps for us 
        DUFFS_MACHINE16(-1, OP, wide_view);
    }
    
    DUFFS_MACHINE16(loop_size, OP, wide_view);
    
#undef OP
    
    const u8 *remainder = (const u8 *)wide_view;
    result += load_wide_int_remainder(size, remainder);
    return result;
}
